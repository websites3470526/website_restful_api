module.exports = app => {
  const projects = require("../controllers/projects.controller");

  //Gets all projects.
  //http://localhost:3001/aa
  app.get("/aa", projects.findAll);

  //Creates new project.
  //http://localhost:3001/aa
  app.post("/aa", projects.create);

  //Gets project info by id.
  //http://localhost:3001/ab?id=3
  app.get("/ab", projects.getById);

  //Updates project info by id.
  //http://localhost:3001/ab?id=2
  app.put("/ab", projects.updateById);

  //Deleted project entry by id.
  //http://localhost:3001/ab?id=2
  app.delete("/ab", projects.deleteById);

  //Gets project info by name.
  //http://localhost:3001/ac?name=CRM System
  app.get("/ac", projects.getByName);

  //Deletes all projects.
  //http://localhost:3001/ac
  app.delete("/ac", projects.deleteAll);
}
