const db = require("./db");

// TODO: complete the code as per the instructions given in Assignment 3


const Project = function(project) {
  this.id = project.id;
  this.projectname = project.projectname;
  this.projectdesc = project.projectdesc;
  this.startdate = project.startdate;
  this.enddate = project.enddate;
};

//Creates new project.
Project.create = (newProject, result) => {

  db.query("INSERT INTO projects SET ?", newProject, (err, res) => {
    if(err){
      console.log("ERROR: ", err);
      result(err, null);
      return;
    }
    console.log("Created project: ", {id: res.insertId, ...newProject});
    result(null, {id: res.insertId, ...newProject});
  });


}

//Gets all project information.
Project.getAll = result => {
  db.query("select * from projects", (err, res) => {

    if(err){
      console.log("error", err);
      result(null, err);
      return;
    }
    console.log("projects: ", res);
    result(null, res);
  });
};

//Gets project by id.
Project.getById = (projectId, result)=> {

  db.query("SELECT * from projects WHERE id = ?", projectId, (err,res) => {

    if(err){
      console.log("ERROR ", err);
      result(err,null);
      return;
    }
    console.log("project information for " + projectId, res);
    result(null, res);
  });
};

//Gets project by name.
Project.getByName = (projectName, result) => {

  db.query("select * from projects where projectname = ?", projectName, (err,res) => {

    if(err){
      console.log("ERROR ", err);
      result(err,null);
      return;
    }
    console.log("project information for name: ", res);
    result(null, res);
  });
};

//Updates project by id.
Project.updateById = (newUpdateById, projectId, result) => {

  db.query("UPDATE projects SET ? WHERE id = ?", [newUpdateById, projectId], (err, res) => {

    if(err){
      console.log("ERROR: ", err);
      result(err, null);
      return;
    }
    console.log("Updated Project: ", newUpdateById);
    result(null, newUpdateById);
  });
};

//Deletes project using id.
Project.deleteById = (newDeleteById, result) => {

  db.query("DELETE from projects WHERE id = ?", newDeleteById, (err,res) => {

    if(err){
      console.log("ERROR: ", err);
      result(err, null);
      return;
    }
    console.log("Deleted project: ", newDeleteById);
    result(null, newDeleteById);
  });
};

//Deletes all projects.
Project.deleteAll = (result) => {

  db.query("Delete from projects", (err, res) => {

    if(err){
      console.log("ERROR: ", err);
      result(err, null);
      return;
    }
    console.log("All projects deleted. ", res);
    result(null, res);

  });


};
module.exports = Project;