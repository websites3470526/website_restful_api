const Project = require("../models/projects.model");

//Get all projects in database.
exports.findAll = (req, res) =>{

    Project.getAll((err, data) =>{
        if(err){
            res.status(500).send({
                message: 
                    err.message || "Error has occured while getting projects."

            });
        }
        else res.send(data);
    });
};

//Create new project in database.
exports.create = (req, res) =>{

    if(!req.body){
        res.status(400).send({

            message: "Content can not be empty"
        });
    }

    const project = new Project({
        id: req.body.id,
        projectname: req.body.projectname,
        projectdesc: req.body.projectdesc,
        startdate: req.body.startdate,
        enddate: req.body.enddate
    });

    Project.create(project, (err, data) => {
        if(err)
            res.status(500).send({

                message:
                    err.message || "Error occured while creating project."
            });
        else res.send(data);
    });
};

//Get project by id
exports.getById = (req, res) => {
    const id = req.query.id;
    Project.getById(id, (err,data) => {
        if(err){
            res.status(500).send({
                message:
                    err.message || "Error occurred while fetching projects with given id."
            });
        }
        else res.send(data);
    });
};

//Get project by name.
exports.getByName = (req, res) => {
    const name = req.query.name;
    Project.getByName(name, (err, data) => {
        if(err){
            res.status(500).send({
                message:
                    err.message || "Error occurred while fetching projects with given name."
            });
        }
        else res.send(data);
    });
};

//Update project by id.
exports.updateById = (req,res) => {
    
    if(!req.body){
        res.status(400).send({
            message: "Content can not be empty."
        });
    }

    const id = req.query.id;

    const project = new Project({
        id: req.query.id,
        projectname: req.body.projectname,
        projectdesc: req.body.projectdesc,
        startdate: req.body.startdate,
        enddate: req.body.enddate
    });
    Project.updateById(project, id, (err,data) => {

        if(err){
            res.status(500).send({
                message:
                    err.message || "Error occured while updating project."
            });
        }
        else res.send(data);

    });
};

//Delete project using id.
exports.deleteById = (req, res) => {

    if(!req.body){
        res.status(400).send({
            message: "Content can not be empty."
        });
    }

    const id = req.query.id;

    Project.deleteById(id, (err,data) => {

        if(err){
            res.status(500).send({
                message:
                    err.message || "Error occured while deleting project."
            });
        }
        else res.send(data);
    });
};

//Delete all projects.
exports.deleteAll = (req, res) => {

    Project.getByName((err, data) => {
        if(err){
            res.status(500).send({
                message:
                    err.message || "Error while deleting projects."
            });
        }
        else res.send(data);
    });
};

